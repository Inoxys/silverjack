<?php 
    $cardsID = array();
    
    $playerImgPath = array();
    $playerImgPath[0] = "https://s-media-cache-ak0.pinimg.com/736x/2f/05/a2/2f05a28a65541e56562c9af418a8e95f.jpg";
    $playerImgPath[1] = "https://s-media-cache-ak0.pinimg.com/originals/ce/ec/76/ceec7690fbe488a91d5538e7d7206c37.jpg";
    $playerImgPath[2] = "http://vignette3.wikia.nocookie.net/disney/images/2/28/Mulannewuk.png/revision/latest?cb=20151211202911";
    $playerImgPath[3] = "http://vignette1.wikia.nocookie.net/disneyprincess/images/0/09/4.png/revision/latest?cb=20130730170904";
    $playerImgPath[4] = "http://vignette2.wikia.nocookie.net/disneyprincess/images/1/1b/Zb.png/revision/latest?cb=20130730171049";
    $playerImgPath[5] = "https://s-media-cache-ak0.pinimg.com/736x/12/97/87/129787c36f0163eb3b10d2918e34693b.jpg";
    $playerImgPath[6] = "https://s-media-cache-ak0.pinimg.com/736x/7d/ed/98/7ded98f455349d5da9bb67bea5282e78.jpg";
    $playerImgPath[7] = "http://vignette4.wikia.nocookie.net/disney/images/2/29/Rapunzel-disney-princess-22935939-267-300.jpg/revision/20130427193836";
    $playerImgPath[8] = "https://s-media-cache-ak0.pinimg.com/originals/75/a8/24/75a824c9cc61a240e65cfade95bec4d0.jpg";
    $playerImgPath[9] = "https://upload.wikimedia.org/wikipedia/en/c/cb/Merida_sexy_redesign.jpg";
    
?>
<!DOCTYPE html>
<html>
    <head>
        <title> Silverjack </title>
        <link rel="stylesheet" href="style.css" type="text/css" />
    </head>
    <body>
        <h1> Silverjack </h1>
        
        <?php

            game();
        
        ?>
    </body>
    
</html>

<?php

function getCards() {
    global $cardsID;
    $cards = array();
    $total = 0;
    for ($i = 0; $i < 6 && ($total < 42 || $i < 4); $i++){
        do {
            $card = getCard();
        } while (!isUnique($card["suit"], $card["number"]));
        array_push($cardsID, array($card["suit"], $card["number"]));
        array_push($cards, $card);
        $total += $card["number"];
    }
    array_unshift($cards, $total);
    return $cards;
}

function getCard() {
    $card = array();
    $file = "Images/";
    $randType = rand(1,4);
    switch($randType){
        case 1:
            $file .= "clubs/";
            break;
        case 2:
            $file .= "diamonds/";
            break;
        case 3:
            $file .= "hearts/";
            break;
        case 4:
            $file .= "spades/";
            break;
    }
    $randCardNumber = rand(1,13);
    $file .= "$randCardNumber.png";
    $card["path"] = $file;
    $card["suit"] = $randType;
    $card["number"] = $randCardNumber;
    return $card;
}

function isUnique($cardSuit, $cardNumber){
    global $cardsID;
    for ($i = 0; $i < count($cardsID); $i++){
        if ($cardsID[$i][0] == $cardSuit 
            && $cardsID[$i][1] == $cardNumber) return false;
    }
    return true;
}

function evaluation($all_cards) {
 
    $max = 0;
    $index = 0;
 
    for ($i = 0; $i < 4; $i++) {

        if ($all_cards[$i][0] > $max && $all_cards[$i][0] < 43) {
            
            $max = $all_cards[$i][0];
            $index = $i;
        }
    }
    
    return $index;
}
    
function game() {
    global $playerImgPath;
    shuffle($playerImgPath);
    
    $all_cards = Array();
    
    echo "<div>";
    
    for ($i = 0; $i < 4; $i++) {
         echo "<ul class='hand'>";
         
         echo "<li> <img class='playerImg' src='" . $playerImgPath[$i] . "' alt='Images/card_back.png'> </li>";
         
         $cards = getCards();
         
         array_push($all_cards, $cards);
         
         for ($j = 1; $j < count($cards); $j++) {
             $card = $cards[$j];
             $path = $card["path"];

             echo "<li> <img class='card' src='$path' alt='Images/card_back.png'> </li>";
            
         }
         
         echo "<li class='score'>" . $cards[0] . "</li>";
         
         echo "</ul>";
         
         echo "<br>";
    }
            
    $winner = evaluation($all_cards);
            
    echo "<span id='winner'>Player " . ($winner + 1) . " wins!</span>";
    
    echo "</div>";
    
    echo "<br>";
    
    echo "<a class='playButton' href='index.php'>Play again</a>";
}


?>